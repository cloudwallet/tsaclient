package io.blocko.coinstack.util;


import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class TestInsecureSslGateway {
	
	@Test
	public void test() throws Exception {
		// JDK 1.6 needs BouncyCastleProvider
		// http://bugs.java.com/bugdatabase/view_bug.do?bug_id=7044060
		if (System.getProperty("java.specification.version").equals("1.6")) {
			TSAGatewayConfig.addBouncyCastleProvider();
		}
		
		System.out.println("## "+this.getClass().getSimpleName());
		
		// insecure
		testInsecureSsl(true, "https://www.pcwebshop.co.uk/");
		testInsecureSsl(true, "https://mms.nw.ru/");
		// secure (catch ssl exception)
		testInsecureSsl(false, "https://www.pcwebshop.co.uk/");
		testInsecureSsl(false, "https://mms.nw.ru/");
    }
	
	public void testInsecureSsl(boolean insecure, String req)
			throws TSAGatewayException {
		System.out.println("- testInsecureSsl("+insecure+"): "+req);
		TSAGatewayClient client = new TSAGatewayClient(req, insecure);
		try {
			System.out.print("  req: ");
			String res = client.requestStamp("");
			int summarizes = 15;
			if (res != null && res.length() > summarizes) {
				res = String.format("%s ... (len=%d)",
						res.substring(0, summarizes), res.length());
			}
			System.out.println("  res: "+res);
			assertNotNull(res);
		} catch (IOException e) {
			String eName = e.getClass().getName();
			System.out.println("  catch: "+eName);
			assertTrue(!insecure && eName.startsWith("javax.net.ssl."));
		}
	}
	
}
