package io.blocko.coinstack.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

public class TestPDFController {

	@Test
	public void testPDF() throws IOException {
		InputStream testFile = this.getClass().getResourceAsStream("test.pdf");
		File tempFile = File.createTempFile("test", "pdf");

		PDFController controller = new PDFController(testFile);
		assertEquals(
				"d9d579f0c4724547db57133d93932a8af22b57ca02ef1ff98632f93e686013a5",
				controller.calculateHash());

		controller.writeStampId(tempFile, "teststamp");

		PDFController controller2 = new PDFController(tempFile);
		assertEquals("teststamp", controller2.getStampId());
		assertEquals(controller.calculateHash(),
				controller2.calculateHashStamped());
	}

//	@Test
//	public void testClient() throws IOException {
//		InputStream testFile = this.getClass().getResourceAsStream("test.pdf");
//		File tempFile = File.createTempFile("test", "pdf");
//
//		PDFController controller = new PDFController(testFile);
//		TSAGatewayClient client = new TSAGatewayClient(
//				"http://127.0.0.1:8080/tsa");
//		String token = null;
//		try {
//			token = client.requestStamp(controller.calculateHash());
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (TSAGatewayException e) {
//			System.out.println(e.getErrorCode());
//			e.printStackTrace();
//		}
//		assertNotNull(token);
//		System.out.println(token);
//		controller.writeStampId(tempFile, token);
//
//		PDFController controller2 = new PDFController(tempFile);
//		assertEquals(token, controller2.getStampId());
//	}
//
//	@Test
//	public void testClient2() throws IOException {
//		// InputStream testFile =
//		// this.getClass().getResourceAsStream("test.pdf");
//		// File tempFile = File.createTempFile("test", "pdf");
//		SyncHttpClient client = new SyncHttpClient(true, 80, 443);
//		client.get("http://google.com", new AsyncHttpResponseHandler() {
//			@Override
//			public void onSuccess(final int statusCode, final Header[] headers,
//					final byte[] response) {
//				System.out.println(new String(response));
//			}
//
//			@Override
//			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
//					Throwable arg3) {
//				arg3.printStackTrace();
//				System.out.println(arg0);
//			}
//		});
//	}

}
