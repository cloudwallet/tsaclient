package io.blocko.coinstack.util;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

public class TestTSAClient {
	
	final static String endpoint = "https://test.tsagateway.io/"; // YOUR_TSAGATEWAY_URL
	final static boolean insecure = false;
	
	@Test
	public void testTSAClient() throws IOException, TSAGatewayException {
		
		InputStream testFile = this.getClass().getResourceAsStream("test.pdf");
		PDFController controller = new PDFController(testFile);
		testFile.close();
		
		String documentHash = controller.calculateHash();
		System.out.println("documentHash:"+documentHash);
		
		
		TSAGatewayClient client = new TSAGatewayClient(endpoint, insecure);
		String stampId = client.requestStamp(documentHash);
		System.out.println("stampId:"+stampId);
	}
}
