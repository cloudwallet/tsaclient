package io.blocko.coinstack.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class TSAGatewayConfig {

	/**
	 * Add BouncyCastle to Security Provider for supporting JDK 1.6
	 * 
	 * @see {@link Security}
	 * @see {@link BouncyCastleProvider}
	 */
	public static void addBouncyCastleProvider() {
		Security.addProvider(new BouncyCastleProvider());
	}
}
