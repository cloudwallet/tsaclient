package io.blocko.coinstack.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;

import cz.msebera.android.httpclient.Header;

public class TSAGatewayClientAsync {
	private String endpoint;
	private AsyncHttpClient client;

	public TSAGatewayClientAsync(String endpoint) {
		this.endpoint = endpoint;
		this.client = new AsyncHttpClient();
	}

	public TSAGatewayClientAsync(String endpoint, boolean insecure) {
		this.endpoint = endpoint;
		this.client = new AsyncHttpClient(insecure, 80, 443);
	}

	public TSAGatewayClientAsync(String endpoint, boolean insecure, int timeout) {
		this.endpoint = endpoint;
		this.client = new AsyncHttpClient(insecure, 80, 443);
		this.client.setTimeout(timeout * 1000);
	}

	public void requestStamp(String hash, final Callback callback) {
		String requestURL = this.endpoint + "?documentHash=" + hash;
		AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
			@Override
			public void onFailure(int status, Header[] headers, byte[] errorResponse, Throwable e) {
				if (null != errorResponse) {
					callback.onIOFailure(new IOException(new String(errorResponse), e));
				} else {
					callback.onIOFailure(new IOException(e));
				}
			}

			@Override
			public void onSuccess(int status, Header[] headers, byte[] response) {
				String responseString = new String(response);
				switch (status) {
				case 200:
					callback.onSuccess(responseString.trim());
					return;
				default:
					callback.onTSAFailure(new TSAGatewayException(status));
					return;
				}
			}
		};
		client.get(requestURL, handler);
	}

	public interface Callback {
		public void onSuccess(String stamp);

		public void onTSAFailure(TSAGatewayException tsaGatewayException);

		public void onIOFailure(IOException ioException);
	}
}
