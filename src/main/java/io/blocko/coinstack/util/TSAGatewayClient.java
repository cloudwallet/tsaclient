package io.blocko.coinstack.util;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.SSLContextBuilder;
import cz.msebera.android.httpclient.conn.ssl.TrustSelfSignedStrategy;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.impl.client.HttpClients;
import cz.msebera.android.httpclient.util.EntityUtils;

public class TSAGatewayClient {
	private String endpoint;
	private CloseableHttpClient client;

	public TSAGatewayClient(String endpoint) {
		this(endpoint, false);
	}
	public TSAGatewayClient(String endpoint, boolean insecure) {
		this.endpoint = endpoint;
		// Creates CloseableHttpClient instance
		if (!insecure) {
			// with default configuration.
			this.client = HttpClients.createDefault();
		}
		else {
		    // with insecure ssl configuration.
		    HttpClientBuilder hcbuilder = HttpClients.custom();
			try {
				// SSLSocketFactory
				SSLContextBuilder ctxbuilder = new SSLContextBuilder()
						.loadTrustMaterial(null, new TrustSelfSignedStrategy());
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
						ctxbuilder.build(),
						SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			    hcbuilder.setSSLSocketFactory(sslsf);
			}
			catch (NoSuchAlgorithmException e) {
				// unreacheable exception
				throw new RuntimeException(e);
			}
			catch (KeyStoreException e) {
				// unreacheable exception
				throw new RuntimeException(e);
			}
			catch (KeyManagementException e) {
				// unreacheable exception
				throw new RuntimeException(e);
			}
		    this.client = hcbuilder.build();
		}
	}

	public String requestStamp(String hash) throws IOException, TSAGatewayException {
		String requestURL = this.endpoint + "?documentHash=" + hash;
		System.out.println(requestURL);
		HttpGet request = new HttpGet(requestURL);
		HttpResponse response = client.execute(request);
		int status = response.getStatusLine().getStatusCode();
		String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
		switch (status) {
		case 200:
			return responseString.trim();
		default:
			throw new TSAGatewayException(status);
		}
	}

}
