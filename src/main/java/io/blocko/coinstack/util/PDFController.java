package io.blocko.coinstack.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.List;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureInterface;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.SignatureOptions;

public class PDFController implements SignatureInterface {

	protected static class StampInfo {

		public static final String SIGNATURE_NAME = "coinstack";
		public static final String SIGNATURE_DELIM = "/";

		String stampId;
		int size;

		public StampInfo(String stampId, int size) {
			this.stampId = stampId;
			this.size = size;
		}

		public static StampInfo loadLegacyWithName(String source) {
			return loadLegacy(source, SIGNATURE_DELIM, 1, 2);
		}

		public static StampInfo loadLegacyWithReason(String source) {
			return loadLegacy(source, SIGNATURE_DELIM, 0, 1);
		}

		public static StampInfo loadLegacy(String source, String delim, int stampIdIndex, int sizeIndex) {
			String[] tokens = source.split(delim);
			if (tokens == null || tokens.length < sizeIndex + 1) {
				throw new RuntimeException("MalformedStampInfo Exception");
			}
			String stampId = tokens[stampIdIndex];
			int size = Integer.valueOf(tokens[sizeIndex]);
			return new StampInfo(stampId, size);
		}

		public static StampInfo load(String stampId, String size) {
			int originalSize = Integer.valueOf(size);
			return new StampInfo(stampId, originalSize);
		}

		public String getStampId() {
			return stampId;
		}

		public int getSize() {
			return size;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(stampId);
			sb.append(SIGNATURE_DELIM);
			sb.append(size);
			return sb.toString();
		}

		public static boolean accept(String source) {
			return (source != null && source.startsWith(SIGNATURE_NAME));
		}
	}

	private byte[] source = null;
	private int size = -1;
	private PDDocument doc = null;
	private StampInfo stamp = null;

	public PDFController(byte[] input) {
		init(input);
	}

	public PDFController(InputStream input) {
		try {
			byte[] buffer = loadStream(input);
			init(buffer);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public PDFController(File input) {
		try {
			InputStream is = new FileInputStream(input);
			byte[] buffer = loadStream(is);
			init(buffer);
			is.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static byte[] loadStream(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(in, out);
		return out.toByteArray();
	}

	private void init(byte[] input) {
		this.source = input;
		this.size = source.length;
		try {
			this.doc = PDDocument.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.stamp = loadStampInfo();
	}

	public void close() {
		this.source = null;
		this.size = -1;
		try {
			if (doc != null) {
				doc.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		this.doc = null;
		this.stamp = null;
	}

	@Override
	public byte[] sign(InputStream content) throws IOException {
		return new byte[0];
	}

	private StampInfo loadStampInfo() {
		try {
			List<PDSignature> list = doc.getSignatureDictionaries();
			for (PDSignature item : list) {
				String name = (item == null) ? null : item.getName();
				if (name == null || !name.startsWith(StampInfo.SIGNATURE_NAME)) {
					continue;
				}
				if (name != null && name.indexOf(StampInfo.SIGNATURE_DELIM) > 0) {
					return StampInfo.loadLegacyWithName(name);
				}
				String reason = item.getReason();
				if (reason != null && reason.indexOf(StampInfo.SIGNATURE_DELIM) > 0) {
					return StampInfo.loadLegacyWithReason(reason);
				}
				String location = item.getLocation();
				return StampInfo.load(reason, location);
			}
			return null;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean hasStampId() {
		return (stamp != null);
	}

	public String getStampId() {
		if (stamp == null) {
			throw new RuntimeException("StampNotFoundException");
		}
		return stamp.getStampId();
	}
	
	String byteArrayToHex(byte[] a) {
	    StringBuilder sb = new StringBuilder();
	    for(final byte b: a)
	        sb.append(String.format("%02x", b&0xff));
	    return sb.toString();
	}

	public String calculateHashStamped() {
		if (stamp == null) {
			throw new RuntimeException("StampNotFoundException");
		}
		MessageDigest sh;
		try {
			sh = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		
		sh.update(source, 0, stamp.getSize());
		byte[] hash = sh.digest();
		return byteArrayToHex(hash);
	}

	public String calculateHash() {
		MessageDigest sh;
		try {
			sh = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
		
		sh.update(source, 0, source.length);
		byte[] hash = sh.digest();
		return byteArrayToHex(hash);
	}

	public void writeStampId(File out, String stampId) throws IOException {
		OutputStream os = new FileOutputStream(out);
		writeStampId(os, stampId);
		os.close();
	}

	public void writeStampId(OutputStream out, String stampId) throws IOException {
		writeStampId(out, stampId, new SignatureOptions());
	}

	public void writeStampId(OutputStream out, String stampId, SignatureOptions options) throws IOException {
		PDSignature signature = new PDSignature();
		signature.setFilter(PDSignature.FILTER_ADOBE_PPKLITE);
		signature.setSubFilter(PDSignature.SUBFILTER_ADBE_X509_RSA_SHA1);
		signature.setName(StampInfo.SIGNATURE_NAME);
		signature.setReason(stampId);
		signature.setLocation(String.valueOf(size));
		signature.setSignDate(Calendar.getInstance());
		doc.addSignature(signature, this, options);
		doc.saveIncremental(out);
	}

}
