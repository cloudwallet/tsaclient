package io.blocko.coinstack.util;

public class TSAGatewayException extends Exception {
	private static final long serialVersionUID = 2364997082032656923L;
	private int status;

	public int getErrorCode() {
		return status;
	}
	
	public TSAGatewayException(int errorCode) {
		this.status = errorCode;
	}

}
